﻿using Emgu.CV;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CYFang
{
    public partial class WebCameraCaptureForm : Form
    {

        /// <summary>
        /// Capture
        /// </summary>
        private Capture _capture;

        /// <summary>
        /// Video Writer
        /// </summary>
        private VideoWriter _videoWriter;

        /// <summary>
        /// Timer
        /// </summary>
        private Timer _timer;

        /// <summary>
        /// FPS
        /// </summary>
        protected virtual int _fps { get { return _capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps) == 0 ? 30 : Convert.ToInt16(_capture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps)); } }

        /// <summary>
        /// Record Video
        /// </summary>
        protected virtual bool _isRecord { get { return this.checkBoxRecover.Checked; } }

        /// <summary>
        /// Web Camera width
        /// </summary>
        protected virtual int _width { get { return this._capture.Width; } }

        /// <summary>
        /// Web Camera height
        /// </summary>
        protected virtual int _height { get { return this._capture.Height; } }

        public WebCameraCaptureForm()
        {
            //Initialize UI
            InitializeComponent();

            //使用第一台WebCam
            _capture = new Capture(0);

            //將畫布寬高調整WebCam視訊大小
            this.pictureBox1.Width = _width;
            this.pictureBox1.Height = _height;

            //使用Timer更新UI以及錄製影片
            _timer = new Timer();
            _timer.Tick += _timer_Tick;
        }

        /// <summary>
        /// 視窗關閉時觸發事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CameraCaptureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stop();
        }


        /// <summary>
        /// 按鈕觸發時事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            //取得當前按鈕名稱
            var text = this.buttonStart.Text;

            //根據按鈕名稱對應方法
            if (text.Equals("啟動"))
                Start();
            else
                Stop();
        }

        /// <summary>
        /// 計時器事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _timer_Tick(object sender, EventArgs e)
        {
            //影像
            var frame = _capture.QueryFrame();

            //將影像轉成Bitmap設定給予畫布
            this.pictureBox1.Image = frame.Bitmap;

            //判斷是否錄製影片
            if (_isRecord)
                //寫入使用者指定檔案
                _videoWriter.Write(frame);
        }

        /// <summary>
        /// 核取方塊狀態改變事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxRecover_CheckStateChanged(object sender, EventArgs e)
        {
            //判斷使用者是否擷取影片
            if (_isRecord)
            {
                //儲存視窗
                var dialog = new SaveFileDialog();

                //儲存檔案類型僅限AVI
                dialog.Filter = "AVI File|*.avi";
                
                //預設資料夾為當前資料夾
                dialog.InitialDirectory = Directory.GetCurrentDirectory();

                //判斷使用者是否決定要選擇擷取影片
                if (dialog.ShowDialog() == DialogResult.OK)
                    _videoWriter = new VideoWriter(dialog.FileName, _fps, new Size(_width, _height), true);
                else
                    checkBoxRecover.Checked = false;
            }
        }


        /// <summary>
        /// 啟動WebCam的方法
        /// </summary>
        private void Start()
        {
            //禁止使用者在選取輸出影片的路徑
            checkBoxRecover.Enabled = false;

            //修改按鈕名稱
            this.buttonStart.Text = "停止";

            //啟動擷取影像
            _capture.Start();

            //計時器啟動
            _timer.Start();
        }

        /// <summary>
        /// 關閉WebCam的方法
        /// </summary>
        private void Stop()
        {
            //關閉計時器
            _timer.Stop();

            //停止影像擷取
            _capture.Stop();

            //釋放擷取影片所用的記憶體
            if (_videoWriter != null)
                _videoWriter.Dispose();

            //讓使用者可以在錄取影片
            checkBoxRecover.Checked = false;
            checkBoxRecover.Enabled = true;
            this.buttonStart.Text = "啟動";
        }


    }
}
